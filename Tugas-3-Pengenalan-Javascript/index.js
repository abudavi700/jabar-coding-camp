//Soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var soal1 = pertama.substr(0,4) + " " + pertama.substr(12,6) + " " + kedua.substr(0,7) + " " + kedua.substr(8,10).toUpperCase();

console.log(soal1);

//Soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var str10 = Number(kataPertama);
var str2 = Number(kataKedua);
var str4 = Number(kataKetiga);
var str6 = Number(kataKeempat);

console.log(str10 + str2 * str4 + str6);

//Soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4,10); // do your own! 
var kataKetiga = kalimat.substr(15,3); // do your own! 
var kataKeempat = kalimat.substr(19,5); // do your own! 
var kataKelima = kalimat.substr(25); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
