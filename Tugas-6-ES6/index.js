// Soal 1

const rumus = (l1, l2)=>{
  //luas
  console.log(`luas persegi panjang adalah ${l1*l2}`);
  //keliling
  console.log(`Keliling persegi panjang adalah ${(l1+l2)*2}`);
};


rumus(5, 4);


// soal 2

const newFunction = (firstName, lastName)=>{
  return {
    firstName,
    lastName,
    fullName: ()=>{
      console.log(`${firstName} ${lastName}`)
    }
  }
}

newFunction("William", "Imoh").fullName();


// Soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby);


//Soal 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west, ...east];

console.log(combined);


// Soal 5

const planet = "earth";
const view = "glass";

var before = (`Lorem ${view} dolor sit amet, consectur adipising elit, ${planet}`);

console.log(before);

