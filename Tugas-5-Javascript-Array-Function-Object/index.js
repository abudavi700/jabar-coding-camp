//Soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftar2 = daftarHewan.sort();


for (i=0; i<5; i++){
  console.log(daftar2[i]);
}

// Soal 2

function introduce(data){
  return (`Nama saya ${data.name}, umur saya ${data.age} tahun, alamat saya di ${data.address}, dan saya punya hobi yaitu ${data.hobby}`);
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };
 
var perkenalan = introduce(data);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// Soal 3

function hitung_huruf_vokal(kata){
  var jumlah = 0;
  var vokal = ['a', 'i', 'u', 'e', 'o'];
  var kata = kata.toLowerCase();
  for (i=0; i<kata.length; i++){
    for (j=0; j<vokal.length; j++){
      if (kata[i] == vokal[j]){
        jumlah++;
      }
    }
  }
  return jumlah;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4

function hitung(angka){
  return angka*2-2;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8































